# All_Matrial_01


These are some **Master Material** nodes In which import our map and do Some manipulations like changing albedo Tint, Metallic, Roughness, Normal, Subsurface Scattering

  

## These are some values we are changing right now

-   **Albedo Tint**
-   **Metallic**
-   **Min/Max Roughness**
-   **Normal intensity**
-   **Alpha Channel (present in Albodo or separate map)**
-   **Drop effect**

  

## In future Update

-   Apply Moss or ice effect on the top or Bottom area
-   Decals master material

  

## How to use that

Convert blurprint project into C++ using Visual Studio


## Instance_matrial
[Instance_matrial](https://gitlab.com/tanwarrohit825/All_Matrial_01/-/tree/main/Content/Matrial/instance_matrial)
**just migrate into your project**
and also cut and paste the UltraDynamicSky folder


## Master Material Present
[Master matrial](https://gitlab.com/tanwarrohit825/All_Matrial_01/-/tree/main/Content/Matrial)

## Contribution
If someone wants to download or also contribute I am really happy 😍
=======
If someone wants to contribute I really happy 😍
